define([
    'jquery',
    'knockout',
    '../avko.components/callbackModalWindow/callbackModalWindow.ViewModel',
    'text!../avko.components/callbackModalWindow/callBackModal.html'
], function ($, ko, callbackModalWindowVM, modalTpl) {

    function createWidget () {

        ko.components.register('callback-modal-window', {
            viewModel: callbackModalWindowVM,
            template: modalTpl
        });
    }

    return {
        createWidget: createWidget
    }
});