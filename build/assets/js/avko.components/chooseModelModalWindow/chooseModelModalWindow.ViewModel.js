define([
    'jquery',
    'knockout',
    'custombox',
    'knockout-jqueryui/selectmenu'
], function ($, ko, custombox) {

    function chooseModelModalWindowVM (params) {
        var self = this;

        self.openModal = function () {
            custombox.open({
                target: '#chooseModelModal',
                effect: 'sign',
                width: '500',
                escKey: true,
                position: ['center', 'center'],
                overlayOpacity: 0.0001
            });
        };

        self.closeModal = function () {
            custombox.close();
        };

        self.carBrands = ko.observableArray([
            { id: '1', text: 'Toyota' },
            { id: '2', text: 'Kia' },
            { id: '3', text: 'Mitsubishi' },
            { id: '4', text: 'Mercedes' },
            { id: '5', text: 'BMW' },
            { id: '6', text: 'Nissan' },
            { id: '7', text: 'Chevrolet' },
            { id: '8', text: 'Hyundai' }
        ]);

        self.carModelItems = ko.observableArray([
            {id: '1', name: 'Hilux'},
            {id: '2', name: 'Prius'},
            {id: '3', name: 'Tundra'},
            {id: '4', name: 'Supra'},
            {id: '5', name: 'Corolla'},
            {id: '6', name: 'Camry'},
            {id: '7', name: 'RAV4'},
            {id: '8', name: 'Avensis'}
        ]);

        self.selectedCarModel = ko.observable('3');

        self.selectedBrand = ko.observable('1');

        params.keepInstance(self);
    }

    return chooseModelModalWindowVM;
});