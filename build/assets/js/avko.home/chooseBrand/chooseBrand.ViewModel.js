define([

], function () {

    var ChooseBrandViewModel = function () {
        var chooseModelModalWindowInstance;

        this.chooseModelFormInvoke = function () {
            chooseModelModalWindowInstance.openModal();
            console.log(this);
        };

        this.getChooseModelViewModel = function (vm) {
            chooseModelModalWindowInstance = vm;
        };

    };

    return ChooseBrandViewModel;
});