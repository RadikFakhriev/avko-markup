define([
    'require',
    'home.config',
    'salesLeaders/salesLeaders',
    'reviews/reviews',
    'requestForm/requestForm',
    'chooseBrand/chooseBrand',
    'header'
], function (require, config) {

    function init () {

        Object.keys(config.subModules).forEach(function (moduleName) {
            (function (item) {
                var currentModuleStr = item + '/' + item,
                    module = require(currentModuleStr);

                if (config.subModules[item].isEnable) {
                    module.init();
                }
            })(moduleName);
        });

        Object.keys(config.common).forEach(function (moduleName) {
            (function (item) {
                var currentModuleStr = item ,
                    module = require(currentModuleStr);

                if (config.common[item].isEnable) {
                    module.init();
                }
            })(moduleName);
        });


    }

    return {
        init: init
    };
});