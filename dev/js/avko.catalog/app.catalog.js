require.config({

    paths: {
        domReady: '../../../vendor/domReady',
        text: '../../../vendor/text',
        jquery: '../../../vendor/jquery',
        knockout: '../../../vendor/knockout',
        slick: '../../../vendor/slick.min'
    },

    shim: {
        jquery: {
            exports: 'jquery'
        },
        knockout: {
            exports: 'knockout'
        },
        slick: {
            exports: 'slick',
            deps: 'jquery'
        }
    },

    priority: ['knockout']
});


require(['knockout', 'catalog', 'domReady!'], function(ko, catalogViewModel) {
    ko.applyBindings(new catalogViewModel());
});