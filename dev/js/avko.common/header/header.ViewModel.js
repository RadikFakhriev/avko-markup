define([

], function () {

    function HeaderViewModel() {
        var callBackModalWindowInstance,
            self = this;

        self.callBackFormInvoke = function () {
            callBackModalWindowInstance.openModal();
        };

        self.getCallbackModalWindowViewModel = function (vm) {
            callBackModalWindowInstance = vm;
        };

    };

    return HeaderViewModel;
});