define([
    'jquery',
    'maskedInput'
], function ($, maskedInput) {

    function init () {
        $('[data-inputmask]').inputmask();
    }

    return {
        init: init
    }

});