define([
    'jquery',
    'knockout',
    'custombox',
    'inputMasksHook'
], function ($, ko, custombox, inputMasksHook) {

    function callbackModalWindowVM (params) {
        var self = this;

        self.phoneNumber = ko.observable(params.initialText || ' ');

        self.openModal = function () {
            custombox.open({
                target: '#callBackModal',
                effect: 'slide',
                width: '490',
                escKey: true,
                position: ['center', 'center'],
                overlayColor: '#fff',
                overlayOpacity: 0.8
            });

            inputMasksHook.init();
        };

        self.closeModal = function () {
            custombox.close();
        };

        self.callbackClaim = function () {
            alert(self.phoneNumber());
            self.closeModal();
        };

        params.keepInstance(self);
    }

    return callbackModalWindowVM;
});