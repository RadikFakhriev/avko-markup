var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    nib = require('nib'),
    livereload = require('gulp-livereload'),
    rimraf = require('gulp-rimraf'),
    concat = require('gulp-concat'),
    spritesmith = require("gulp.spritesmith"),
    mainBowerFiles = require('main-bower-files'),
    print = require("gulp-print"),
    rename = require("gulp-rename"),
    autoPrefixer = require('autoprefixer-stylus');


gulp.task('stylus', function () {
    rimraf('./build/assets/css');
    gulp.src(['./dev/**/*.styl', './dev/*.styl'])
        .pipe(stylus({error: true, use: [nib(), autoPrefixer()]}))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./build/assets/css'))
        .pipe(livereload());
});

gulp.task('js', function () {
    gulp.src(['./dev/js/**'])
        .pipe(gulp.dest('./build/assets/js'))
        .pipe(livereload());
});

gulp.task('html', function () {
    gulp.src(['./dev/*.html'])
        .pipe(gulp.dest('./build/'))
        .pipe(livereload());
});

gulp.task('fonts', function () {
    gulp.src(['./dev/fonts/**'])
        .pipe(gulp.dest('./build/assets/fonts'))
        .pipe(livereload());
});

gulp.task("bower-files", function(){
    var currentDirBuffer = '',
        idxClosePathDir,
        overrideLibsMainFilesConfig = {
            sly: {
                main: [
                    './dist/sly.min.js'
                ]
            },
            'requirejs-domready' : {
                main: [
                    'domReady.js'
                ]
            }
        };

    //2 pipe for divide libs on folders
    gulp.src(mainBowerFiles({
        overrides: overrideLibsMainFilesConfig
    }))
        .pipe(print(function (filepath) {
            //remove 'bower_components\' substring
            currentDirBuffer = filepath.substr(22);
            idxClosePathDir = currentDirBuffer.indexOf("\\");
            currentDirBuffer = currentDirBuffer.substr(0, idxClosePathDir);
        }))
        .pipe(rename(function (filepath) {
            filepath.dirname = currentDirBuffer;
        }))
        .pipe(gulp.dest("./vendor/"));
});

gulp.task('lr-server', function () {
    var lrOptions = {
        port: 35729,
        host: 'localhost',
        start: true
    };
    livereload(lrOptions);
    livereload.listen();
});

gulp.task('images', function () {
    var spriteData =
        gulp.src('./dev/img/*.png')
            .pipe(spritesmith({
                imgName: 'sprite.png',
                imgPath: '../img/sprite.png',
                cssName: 'stylus/avko.common/sprite.styl',
                cssFormat: 'stylus',
                algorithm: 'binary-tree',
                cssVarMap: function(sprite) {
                    sprite.name = 'sprite-' + sprite.name
                }
            }));

    spriteData.img.pipe(gulp.dest('./build/assets/img/'));
    spriteData.css.pipe(gulp.dest('./dev/')).pipe(livereload());
});

gulp.task('pics', function () {
    gulp.src(['./dev/pictures/**'])
        .pipe(gulp.dest('./build/assets/pictures'))
        .pipe(livereload());
});

gulp.task('watch', function () {
    var initiallyTasks = ['lr-server', 'fonts', 'pics', 'images', 'html', 'stylus', 'js'];

    gulp.start(initiallyTasks);

    gulp.watch('./dev/**/*.png',['images']);
    gulp.watch(['./dev/**/*.styl', './dev/*.styl'], ['stylus']);
    gulp.watch('./dev/**/*.html', ['html']);
    gulp.watch('./dev/**/*.js', ['js']);
});

gulp.task('build', function () {
    rimraf('./build');
    gulp.start(['images', 'pics', 'fonts', 'html', 'stylus', 'js']);
});

